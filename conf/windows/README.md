# Installing on Windows
This page outlines the installation of the gitflow scripts on the three major distributions of git. Please follow the directions for your git distribution.

For Windows users, [Git for Windows](https://git-for-windows.github.io/) is a good
starting place for installing git. Git for Windows also includes Git-flow.

## Cygwin

For Windows users who wish to use the automated install, it is suggested that you install [Cygwin](http://www.cygwin.com/)
first to install tools like `git`, `util-linux` and `wget` (with those three being packages that can be selected
during installation). Then simply run this command from a Cygwin shell in your `$HOME`:

	$ ./gitflow-installer.sh

If you are on a restricted user account, you can switch the installation location with the INSTALL_PREFIX environment variable:

        $ export INSTALL_PREFIX=$USERPROFILE/bin
        # Run wget command above

Of course, the location you install to should be in `$PATH`

If you get the error "flags: FATAL unable to determine getopt version" error after 

	$ git flow init

you need to install the `util-linux` package using the Cygwin setup.

If you get something like "$'\r': command not found" then it's a problem with your line endings.  You should run the following:

	$  sed -i 's/\n\r/\n/mg' /usr/local/bin/git-flow*
	$  sed -i 's/\n\r/\n/mg' /usr/local/bin/gitflow-*

