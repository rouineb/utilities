#!/usr/bin/env sh

ENCODED_ART='ICAgX19fX18gX19fX18gIF8gICAgICAgIF9fX19fX18gX19fX19fICAgICAgICAgIF9fICBfXyAKICAvIF9fX198ICBfXyBcfCB8ICAgICAgfF9fICAgX198ICBfX19ffCAgIC9cICAgfCAgXC8gIHwKIHwgfCAgICB8IHxfXykgfCB8ICAgICAgICAgfCB8ICB8IHxfXyAgICAgLyAgXCAgfCBcICAvIHwKIHwgfCAgICB8ICBfX18vfCB8ICAgICAgICAgfCB8ICB8ICBfX3wgICAvIC9cIFwgfCB8XC98IHwKIHwgfF9fX198IHwgICAgfCB8X19fXyAgICAgfCB8ICB8IHxfX19fIC8gX19fXyBcfCB8ICB8IHwKICBcX19fX198X3wgICAgfF9fX19fX3wgICAgfF98ICB8X19fX19fL18vICAgIFxfXF98ICB8X3wKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAK'

install_python3() {
    # get_os_type
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
        sudo apt update && sudo apt install -y python3.6 python3-pip
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        echo "Mac OS not yet implemented !"
    elif [[ "$OSTYPE" == "cygwin" ]] || [[ "$OSTYPE" == "msys" ]] || [[ "$OSTYPE" == "win32" ]]; then
        # POSIX compatibility layer and Linux environment emulation for Windows
        echo "Detecting $OSTYPE : compatible, downloading python3 ..."
        wget --no-check-certificate https://www.python.org/ftp/python/3.6.5/python-3.6.5.exe -O python3.6.5.exe
        echo "Just run it and relaunch the terminal !"
    elif [[ "$OSTYPE" == "alpine" ]]; then
        apk add python3
    else
        echo "Uknown OS Type !"
        exit
    fi
}

get_os_type() {
    OSTYPE=$(cat /etc/os-release | awk 'NR==2{split($1,content,"="); print content[2]}')
}

echo $ENCODED_ART | base64 -d
# check if python 3 is installed
if [ -z "$https_proxy" ]; then
    echo -e "HTTPS environment variable is not set\n\tHOW TO : $ export https_proxy=PROXY_URL"
    echo -e "If you need help, have a look at helper project in https://apps.bsc.aws.societegenerale.com/gitlab/CPL/utils#master"
fi

install_python3
# python3 --version &>/dev/null
# if [ $? -ne 0 ]; then
#     echo "\nIt seems you didn't install python3, would you like to install it ?[y/n]"
#     read answer
#     if [ "$answer" == "y" ]; then
#         install_python3
#     else
#         echo "Assuming you've already installed python3 !"
#     fi
# fi
if [ ! -z "$https_proxy" ]; then
    options="--proxy $https_proxy"
fi
pip3 install --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org -r requirements.txt $options
