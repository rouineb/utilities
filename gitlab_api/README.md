# Easy_Gitlab Package

This is a simple script to help Teams follow a git/gitlab best practices

## DESCRIPTION

This is a simple script meant for CPL TEAM to create s/features branches correctly
Thus no longer losing time doing basic configuration. In other words, the developer when
working on a new feature that belongs to a new/existing spring.

## OPTIONS

- -s, --s:
  Specify the s number of the s branch to be created.
- -p, --project:
  Put the project name for s/feature branches to use
- -i, --init:
  Generate the configuration file for easy gitlab, else the script will not work, this is a mandatory first step

## EXAMPLES

One of the best ways to undertand a new tool is to play with it and tinker it's inner functions
Let's do that, by running together few basic examples as follow:

1.  Initiate the configuration: ./easy_gitlab -i/--init
    This will create easy_gitlab's ini configuration file ~/.python-gitlab.cfg
    please be careful when tweaking the features, it's a mandatory file for this tool !

2.  Launch a new s : ./easy_gitlab -s [0-9]+ -p cpl|gift
    This will create a new s named following the property 's_branch_name_pattern' specified in the config file

3.  Launch a new feature : ./easy_gitlab -f [a-zA-Z]{{5,80}} -p cpl|gift
    It will create a new feature branch named following the property 'feature_branch_name_pattern' specified in the config file
    IMPORTANT
    If you are in a restricted network area and you need to use a proxy, then you'll have to
    to define HTTP_PROXY and HTTPS_PROXY environment variables
    GITLAB URL
    Deep inside you, if you think you can make this script even better or need some more work, please I'm all ears
    I'll be more than happy, just contribute and push forward our work.
