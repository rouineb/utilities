from enum import Enum


class Ini_sections(Enum):
    GLOBAL = 'global'
    SG = 'sg'
    BRANCHES = 'branches'


class Ini_Branches_Keys(Enum):
    URL = 'url'
    OAUTH_TOKEN = 'oauth_token'
    SPRINT_BRANCH_NAME_PATTERN = 'sprint_branch_name_pattern'
    SPRINT_BASE_BRANCH_NAME = 'sprint_base_branch_name'
    BASE_BRANCH_NAME = 'base_branch_name'
    BRANCH_NAME = 'branch_name'
    SPRINT_BRANCH_NAME = 'sprint_branch_name'
    FEATURE_BRANCH_NAME_PATTERN = 'feature_branch_name_pattern'
    FEATURE_BRANCH_NAME = 'feature_branch_name'
    PROTECTED_BRANCHES = 'protected_branches'
    PROTECTED_BRANCHES_PREFIX = 'protected_branches_prefix'
    PROTECTED_BRANCHE_ARCHIVE_PREFIX = 'protected_branch_archive_prefix'
    SPRINT_DURATION = 'sprint_duration'


class SystemCode(Enum):
    ERROR = -1


class Projects(Enum):
    '''
     Instead of calling each time Gitlab api to retrieve 
     projects IDs, and knowing that it will not change
     I've made it as enum
    '''
    CPL_NAME = 'CPL'
    GIFT_NAME = 'GIFT'
    CPL_FRONT_ID = 269
    CPL_BACK_ID = 420
