import gitlab
from gitlab import exceptions
import configparser
import os
from utilities import is_hidden, generate_ini, map_to_ini, map_to_config
from enums import SystemCode, Ini_sections, Ini_Branches_Keys, Projects
import logging
import logging.config
from cachetools import cached, TTLCache
import timeit
from datetime import datetime, timezone, timedelta
import time
from datetime_z import parse_datetime
import requests
import sys


class GitlabConfiguror:
    DEFAULT_INI = {'branches':
                   {'sprint_duration': '14',
                    'protected_branch_archive_prefix': 'archive',
                    'protected_branches': 'develop, master, develop-gift',
                    'protected_branches_prefix': 'feature, protected, hotfix, release',
                    'sprint_branch_name_pattern': 'SPRINT_{sprint_number}/{project}',
                    'sprint_base_branch_name':
                    'protected/develop',
                    'feature_branch_name_pattern': 'FEATURE/SPRINT_{sprint_number}/{desc}'},
                   'global':
                   {'default': 'sg', 'ssl_verify': 'false', 'timeout': '5'},
                   'sg': {'url': 'https://apps.bsc.aws.societegenerale.com/gitlab/',
                          'oauth_token': 'CHANGE_ME_WITH_YOUR_ACCESS_TOKEN', 'api_version': '4'}}

    OUTPUT_FILE_NAME = '.python-gitlab.cfg'
    LOGGER_NAME = 'gitlabApi'
    BRANCH_SEPARATOR = '/'
    FEATURE_PREFIX = 'FEATURE'
    SPRINT_PREFIX = 'SPRINT'

    # 5 minutes cache with 50 max objects
    CACHE = TTLCache(ttl=300, maxsize=50)

    def __init__(self, file_name=OUTPUT_FILE_NAME,
                 logger_config_file='config/logging.conf'):
        self.home = os.path.expanduser('~')
        self._config_parser = self.init(file_name)
        self._gitlab = self.connect()
        logging.config.fileConfig(os.path.expanduser(logger_config_file))
        self._logger = logging.getLogger(GitlabConfiguror.LOGGER_NAME)

    def init(self, file_name):
        self.verify(file_name)
        config = configparser.ConfigParser()
        config.read(os.path.join(self.home, file_name))
        return config

    def verify(self, file_name):
        if not os.path.exists(os.path.join(self.home, file_name)):
            print('{0}/{1} does not exist'.format(self.home, file_name))
            sys.exit(SystemCode.ERROR.value)
        if not is_hidden(file_name):
            print('{0} is not hidden'.format(file_name))
            sys.exit(SystemCode.ERROR.value)

    @property
    def logger(self):
        return self._logger

    @property
    def gitlab(self):
        return self._gitlab

    @staticmethod
    def get_proxies():
        http_proxy = os.environ.get("HTTP_PROXY")
        https_proxy = os.environ.get("HTTPS_PROXY")
        return (http_proxy, https_proxy)

    def verify_sections(self, section):
        '''
            Check if the sg section is present
            This method should be used only in strict mode
        '''
        if section.value not in self._config_parser.sections():
            self._logger.error('%s is not found', section.value)
            # print('${0} is not found', section.value)
            sys.exit(SystemCode.ERROR.value)

    @staticmethod
    def generate_default():
        """
          To generate the scaffold of Gitlab's made easy script
          It will create an Ini file called ~/.python-gitlab.cfg
          that should be tinkered to reflect your own user
        """
        map_to_ini(map_to_config(GitlabConfiguror.DEFAULT_INI), os.path.expanduser(
            '~/{0}'.format(GitlabConfiguror.OUTPUT_FILE_NAME)))
        print('~/{0} has been generated'.format(
              GitlabConfiguror.OUTPUT_FILE_NAME))

    @cached(CACHE)
    def get_property(self, section, property, is_raw=False):
        return self._config_parser.get(section, property, raw=is_raw)

    def connect(self):
        gitlab_url = self._config_parser.get(
            Ini_sections.SG.value, Ini_Branches_Keys.URL.value)
        access_token = self._config_parser.get(
            Ini_sections.SG.value, Ini_Branches_Keys.OAUTH_TOKEN.value)
        # If the proxy is set then use it
        (http, https) = GitlabConfiguror.get_proxies()
        if http and https:
            session = requests.Session()
            session.proxies = {
                'http': http,
                'https': https
            }
            return gitlab.Gitlab(gitlab_url, access_token, session=session)
        return gitlab.Gitlab(gitlab_url, access_token)

    def find(self, group, project_name):
        if group and project_name not in [pr.name
                                          for pr in group.projects.list()]:
            find(group.subgroups.list(), project_name)

    @cached(CACHE)
    def projects(self):
        return self._gitlab.projects.list()

    @cached(CACHE)
    def projects(self, group_id):
        start = timeit.default_timer()
        projects = self._gitlab.groups.get(group_id).projects.list()
        self.logger.debug('Retrieving projects of group %d has taken %s',
                          group_id, timeit.default_timer() - start)
        return projects

    @cached(CACHE)
    def project(self, project_id):
        return self._gitlab.projects.get(project_id)

    @cached(CACHE)
    def groups(self):
        return self._gitlab.groups.list()

    def remove_branch(self, project_ref, branch_name, protected=False):
        if protected:
            project_ref.branches.get(branch_name).unprotect()
        return project_ref.branches.delete(branch_name)

    def create_branch_low_level(self, project_ref, branch_name, branch_ref):
        return project_ref.branches.create({'branch': branch_name,
                                            'ref': branch_ref})

    def create_branch(self, project_ref, properties, protected=False,
                      project_name='CPL'):
        try:
            branch = self.create_branch_low_level(project_ref,
                                                  properties[Ini_Branches_Keys.BRANCH_NAME.value],
                                                  properties[Ini_Branches_Keys.BASE_BRANCH_NAME.value])
            if protected:
                branch.protect(developers_can_push=False,
                               developers_can_merge=True)
            else:
                branch.unprotect()

            self._logger.debug('%s branch %s was created in %s',
                               'Protected' if protected else 'Non protected',
                               properties['branch_name'], project_ref.name)
        except exceptions.GitlabCreateError as ex:
            self._logger.error("Error creating sprint branch for project {0} : {1}".format(
                project_ref.id, ex.error_message))
        except exceptions.GitlabHttpError as ex:
            self._logger.error("Http Error while creating sprint branch for project {0} : {1}".format(
                project_ref.id, ex.error_message))

    def create_sprint_branch(self, project_id, sprint_number, protected=True, project_name='CPL'):
        project = self.project(project_id)
        sprint_branch_name_pattern = self.get_property(
            Ini_sections.BRANCHES.value,
            Ini_Branches_Keys.SPRINT_BRANCH_NAME_PATTERN.value)
        sprint_base_branch_name = self.get_property(
            Ini_sections.BRANCHES.value,
            Ini_Branches_Keys.SPRINT_BASE_BRANCH_NAME.value)
        sprint_branch_name = sprint_branch_name_pattern.format(
            sprint_number=sprint_number,
            project=project_name
        )
        self.create_branch(project, {
            Ini_Branches_Keys.BRANCH_NAME.value: sprint_branch_name,
            Ini_Branches_Keys.BASE_BRANCH_NAME.value: sprint_base_branch_name
        }, protected, project_name)

    def create_feature_branch(self, project_id, sprint_number, desc, project_name='CPL'):
        project = self.project(project_id)
        feature_branch_name_pattern = self.get_property(
            Ini_sections.BRANCHES.value,
            Ini_Branches_Keys.FEATURE_BRANCH_NAME_PATTERN.value)
        feature_branch_name = feature_branch_name_pattern.format(
            sprint_number=sprint_number, desc=desc.lower())
        sprint_branch_name_pattern = self.get_property(
            Ini_sections.BRANCHES.value,
            Ini_Branches_Keys.SPRINT_BRANCH_NAME_PATTERN.value)
        sprint_branch_name = sprint_branch_name_pattern.format(
            sprint_number=sprint_number, project=project_name)
        self.create_branch(project, {
            Ini_Branches_Keys.BRANCH_NAME.value: feature_branch_name,
            Ini_Branches_Keys.BASE_BRANCH_NAME.value: sprint_branch_name
        }, False, project_name)

    def remove_sprint_branch(self, project_id, sprint_number, project_name='CPL'):
        try:
            project = self.project(project_id)
            sprint_branch_name_pattern = self.get_property(
                Ini_sections.BRANCHES.value,
                Ini_Branches_Keys.SPRINT_BRANCH_NAME_PATTERN.value)
            sprint_branch_name = sprint_branch_name_pattern.format(
                sprint_number=sprint_number, project=project_name)
            self.remove_branch(project, sprint_branch_name, True)
            self._logger.debug('%s was delete from %s',
                               sprint_branch_name, project.name)
        except exceptions.GitlabDeleteError as ex:
            self._logger.error("Error deleting sprint branch with number {0} for project {1} : {2}".format(
                sprint_number, project_id, ex.error_message))

    def remove_feature_branch(self, project_id, sprint_number, desc, project_name='CPL'):
        try:
            project = self.project(project_id)
            feature_branch_name_pattern = self.get_property(
                Ini_sections.BRANCHES.value,
                Ini_Branches_Keys.FEATURE_BRANCH_NAME_PATTERN.value)
            feature_branch_name = feature_branch_name_pattern.format(
                sprint_number=sprint_number, desc=desc.lower())
            sprint_branch_name_pattern = self.get_property(
                Ini_sections.BRANCHES.value,
                Ini_Branches_Keys.SPRINT_BRANCH_NAME_PATTERN.value)
            sprint_branch_name = sprint_branch_name_pattern.format(
                sprint_number=sprint_number, project=project_name)
            self.remove_branch(project, sprint_branch_name)
            self._logger.debug('%s was delete from %s',
                               feature_branch_name, project.name)
        except exceptions.GitlabDeleteError as ex:
            self._logger.error("Error deleting sprint branch with number {0} for project {1} : {2}".format(
                sprint_number, project_id, ex.error_message))

    def sprint_features_branches(self, project_id, sprint_number):
        project = self.project(project_id)
        features = filter(lambda project_branch: project_branch.name.startswith(
            GitlabConfiguror.FEATURE_PREFIX), project.branches.list())
        sprinted_features = list()
        for project_branch in features:
            destructed_branch_name = project_branch.name.split('/')
            if destructed_branch_name[1] == 'SPRINT_{0}'.format(sprint_number):
                sprinted_features.append(project_branch)
        self._logger.debug(sprinted_features)
        return sprinted_features

    def merge_request(self, project_id, source_branch, target_branch, title, labels=['CPL', 'AUTOMATIC']):
        project = self.project(project_id)
        return project.mergerequests.create({
            'source_branch': source_branch,
            'target_branch': target_branch,
            'title': title,
            'labels': labels
        })

    def sprint_finish(self, project_id, sprint_number, project_name='CPL'):
        sprinted_features = self.sprint_features_branches(
            project_id, sprint_number)
        sprint_branch_name_pattern = self.get_property(
            Ini_sections.BRANCHES.value,
            Ini_Branches_Keys.SPRINT_BRANCH_NAME_PATTERN.value)
        sprint_branch_name = sprint_branch_name_pattern.format(
            sprint_number=sprint_number, project=project_name)
        for branch in sprinted_features:
            try:
                mr = self.merge_request(project_id, branch.name, sprint_branch_name,
                                        '[MERGE][Sprint {0} is finished]'.format(sprint_number))
                mr.description
                'Merge request created using python script since Sprint {0} is finished'.format(
                    sprint_number)
                mr.save()
                self._logger.debug(
                    'Merge request created between branches : %s -> %s',
                    branch.name, sprint_branch_name)
            except exceptions.GitlabCreateError as ex:
                self._logger.error("Http Error while creating merge request for project {0} : {1}".format(
                    project_id, ex.error_message))
                SystemExit(SystemCode.ERROR.value)

    def get_sprint_branches(self, project_id):
        """
           Grabs all sprint branches in a given project. Note that
           the REGEX used to compare and validate branche name is based
           solely on ini config file, specifically the sprint_branch_name_pattern property
           in branches section.

           project_id: Gitlab's project id
           return: list of ProjectBranch objects

           :Example:
                >>> branches_names = map(lambda branch: branch.name, get_sprint_branches(269))
                >>> print(branches_names)
                ['SPRINT_30/CPL','SPRINT_28/CPL','SPRINT_40/GIFT']
        """
        project = self.project(project_id)
        return filter(lambda branch: GitlabConfiguror.SPRINT_PREFIX in branch.name.split(GitlabConfiguror.BRANCH_SEPARATOR)[0],
                      project.branches.list())

    def guess_sprint(self, project_id):
        """
            Helps guessing sprint number, meaning, if the user already initiated
            beforehand a sprint, we'll get it, if not a NOT_FOUND (-1) value
            will be returned
            project_id: Gitlab's project id
            return: -1 if not able to guess, otherwise, it will return Sprint Number
            :rtype: ProjectBranch

            :Example:
            Imagine you've already initiated Sprint 31, in this case,
            branches SPRINT_31/awesome_feature is created and hopefully other feature branches
            created based on it.
            In this case, if you run this script, it will return 31 as the Most Recently Created
            Sprint.

            .. warning:: If no Sprint branches are located, ValueError will be thrown
        """
        sprint_branches = self.get_sprint_branches(project_id)
        return min(sprint_branches, key=lambda branch: branch.commit['created_at'])

    def clean_branches(self, project_id, force=False):
        """
            Cleans unused/outdated branches based on the created_at attribute of the branch.
            This functions will ignore all protected branches specified in the configuration file
            through properties :

                * protected_branches : branches that do not follow the convention, you can keep them
                                       but should be added here
                * protected_branches_prefix: all prefixes that should be respected

            project_id: Gitlab's project id

            force: force removing branches that exceeds sprint duration and are not included in the above properties
        """
        project = self.project(project_id)
        project.delete_merged_branches()
        protected_branches = [branch.strip() for branch in self.get_property(
            Ini_sections.BRANCHES.value, Ini_Branches_Keys.PROTECTED_BRANCHES.value).split(',')]
        protected_branches_prefix = [branch.strip() for branch in self.get_property(
            Ini_sections.BRANCHES.value, Ini_Branches_Keys.PROTECTED_BRANCHES_PREFIX.value).strip().split(',')]
        branches_to_delete = filter(
            lambda branch: branch.name.lower().split('/')[0] not in protected_branches_prefix, filter(
                lambda project_branch: project_branch.name.lower() not in protected_branches, project.branches.list()))

        now = datetime.now(timezone.utc)
        sprint_duration = self.get_property(Ini_sections.BRANCHES.value,
                                            Ini_Branches_Keys.SPRINT_DURATION.value)
        for branch in branches_to_delete:
            elapsed = now - parse_datetime(branch.commit['created_at'])
            if elapsed < timedelta(days=int(sprint_duration)):
                self.logger.info(
                    'Branch {0} has an age of {1} days and {2} seconds, it will {3} removed as (force={4}), it exceeds sprint duration {5} {6}'
                    .format(branch.name, elapsed.days, elapsed.seconds, 'be' if force else 'not be', force, sprint_duration, 'days' if int(sprint_duration) > 1 else 'day'))
                if force:
                    if branch.protected:
                        branch.unprotect()
                    branch.delete()

    def create_new_sprint_branch_cpl_front(self, sprint_number):
        self.create_sprint_branch(
            Projects.CPL_FRONT_ID.value, sprint_number)

    def create_new_feature_branch_cpl_front(self, sprint_number, desc):
        self.create_feature_branch(
            Projects.CPL_FRONT_ID.value, sprint_number, desc)

    def remove_spring_branch_cpl_front(self, sprint_number):
        self.remove_sprint_branch(Projects.CPL_FRONT_ID.value, sprint_number)

    def finish_sprint_cpl_front(self, sprint_number):
        self.sprint_finish(Projects.CPL_FRONT_ID.value, sprint_number)

    def create_new_sprint_branch_cpl_back(self, sprint_number):
        self.create_sprint_branch(Projects.CPL_BACK_ID.value, sprint_number)

    def remove_spring_branch_cpl_back(self, sprint_number):
        self.remove_sprint_branch(Projects.CPL_BACK_ID.value, sprint_number)

    def create_new_feature_branch_cpl_back(self, sprint_number, desc):
        self.create_feature_branch(
            Projects.CPL_BACK_ID.value, sprint_number, desc)


if __name__ == "__main__":
    configuror = GitlabConfiguror('.python-gitlab.cfg')
