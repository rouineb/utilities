import os
from enums import SystemCode
import configparser


def is_hidden(file_path):
    name = os.path.basename(os.path.abspath(file_path))
    return name.startswith('.')


def create_file(file_path, mode):
    '''
      General purpose function to create a given file at a specific location
    '''
    try:
        file = open(file_path, mode)
        file.close()
    except FileExistsError:
        print('${0} file already exists', file_path)
        SystemExit(SystemCode.ERROR.value)


def ini_to_map(ini_file_path):
    config = configparser.ConfigParser()
    config.read(ini_file_path)
    return {section: dict(config.items(section)) for section in config.sections()}


def map_to_ini(config, output_file_path):
    with open(output_file_path, 'w') as configfile:
        config.write(configfile)


def map_to_config(map):
    config = configparser.ConfigParser()
    for k, v in map.items():
        config[k] = v
    return config


def generate_ini(output_file_path, default_config='config/default_config.cfg'):
    '''
        Generate an ini file based on a default configuration,
        note that the file is generated X times as much you call this function
    '''
    config = configparser.ConfigParser()
    config.read(default_config)
    with open(output_file_path, 'w') as configfile:
        config.write(configfile)
