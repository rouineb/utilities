#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyfiglet
from api import GitlabConfiguror
import getopt
import sys


def help():
    print("NAME")
    print("\teasy_gitlab")
    print("\nSYNOPSIS")
    print(
        "\t{0} [-hvfs] [-s --sprint] [-f --feature] [-c config_file_path] [--config=config_file_path]".format(sys.argv[0]))
    print("\nDESCRIPTION")
    print("\tThis is a simple script meant for CPL TEAM to create sprint/features branches correctly")
    print("\tThus no longer losing time doing basic configuration. In other words, the developer when")
    print("\tworking on a new feature that belongs to a new/existing spring.")
    print("\nOPTIONS")
    print("\t -s, --sprint:")
    print("\t\t Specify the sprint number of the sprint branch to be created.")
    print("\t -p, --project:")
    print("\t\t Put the project name for sprint/feature branches to use")
    print("\t -i, --init:")
    print("\t\t Generate the configuration file for easy gitlab, else the script will not work, this is a mandatory first step")
    print("\nEXAMPLES")
    print("\tOne of the best ways to undertand a new tool is to play with it and tinker it's inner functions")
    print("\tLet's do that, by running together few basic examples as follow: ")
    print(
        "\n\t\t 1. Initiate the configuration: {0} -i/--init".format(sys.argv[0]))
    print("\t\t This will create easy_gitlab's ini configuration file ~/.python-gitlab.cfg")
    print("\t\t please be careful when tweaking the features, it's a mandatory file for this tool !")
    print(
        "\n\t\t 2. Launch a new sprint : {0} -s [0-9]+ -p (cpl|gift)".format(sys.argv[0]))
    print("\t\t This will create a new sprint named following the property 'sprint_branch_name_pattern' specified in the config file")
    print(
        "\n\t\t 3. Launch a new feature : {0} -f [a-zA-Z]{{5,80}} -p (cpl|gift)".format(sys.argv[0]))
    print("\t\t It will create a new feature branch named following the property 'feature_branch_name_pattern' specified in the config file")
    print("\nIMPORTANT")
    print("\tIf you are in a restricted network area and you need to use a proxy, then you'll have to")
    print("\tto define HTTP_PROXY and HTTPS_PROXY environment variables")
    print("\nGITLAB URL")
    print("\tDeep inside you, if you think you can make this script even better or need some more work, please I'm all ears")
    print("\tI'll be more than happy, just contribute and push forward our work.")
    print("\thttps://gitlab.com/devlifealways/utilities.git")
    print("\nAuthor(s):")
    print("\tROUINEB Hamza : rouineb.work@gmail.com")


def main(argv):
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hifs:p:v", [
                                   "help", "init", "sprint=", "project=", "force"])
    except getopt.GetoptError as err:
        print(err)
        print("For more insight, run {0} --help".format(sys.argv[0]))
        sys.exit(2)
    output = None
    verbose = False
    feature = ''
    configFile = ''
    project = ''
    sprint_number = -1
    if len(opts) == 0:
        help()
        sys.exit()
    for o, a in opts:
        if o == "-v":
            verbose = True
        elif o in ("-h", "--help"):
            help()
            sys.exit()
        elif o in ("-f", "--feature"):
            feature = a.lower()
        elif o in ("-i", "--init"):
            GitlabConfiguror.generate_default()
            print("Don't forget to change the default values")
            sys.exit()
        elif o in ("-p", "--project"):
            if a.lower() != 'gift' and a.lower() != 'cpl':
                print("The only acceptable values are cpl/gift for the time being")
                sys.exit(-1)
            else:
                project = a.upper()
        elif o in ("-s", "--sprint"):
            sprint_number = int(a)
            if sprint_number < 0:
                print("Sprint number cannot be a negative number !")
                sys.exit(-1)
        else:
            assert False, "unhandled option"
    print(pyfiglet.figlet_format("CPL TEAM"))
    configuror = GitlabConfiguror()
    if sprint_number > 0 and not project:
        print("Project option is mandatory when trying to create a new sprint")
        print("For more insight, run {0} --help".format(sys.argv[0]))
    elif project and sprint_number == -1:
        print("Sprint option is mandatory when trying to create a new sprint")
        print("For more insight, run {0} --help".format(sys.argv[0]))
    elif project and sprint_number and not feature:
        configuror.create_new_sprint_branch_cpl_front(sprint_number)
        # configuror.create_sprint_branch(269, sprint_number, True, project)
    elif sprint_number and feature:
        configuror.create_new_feature_branch_cpl_front(sprint_number, feature)
    else:
        print("You cannot run all of the options at once")
        print("For more insight, run {0} --help".format(sys.argv[0]))


if __name__ == "__main__":
    main(sys.argv[1:])
    # configuror = GitlabConfiguror('.python-gitlab.cfg')
    # configuror = GitlabConfiguror()
    # GitlabConfiguror.generate_default()
    # print(configuror.groups())
    # for p in configuror.projects(125):
    #     print('{0} {1}'.format(p.name, p.id))
    # configuror.create_new_sprint_branch_cpl_front(31)
    # configuror.create_new_feature_branch_cpl_front(31, 'python_test_script')
    # configuror.remove_spring_branch_cpl_front(31)
    # print(configuror.sprint_features_branches(269, 31))
    # for x in configuror.get_sprint_branches(269):
    #     print(x.commit['created_at'])
    # configuror.finish_sprint_cpl_front(31)
    # configuror.clean_branches(269)
    # print(configuror.guess_sprint(269).name)
    # configuror.clean_branches(269, True)
