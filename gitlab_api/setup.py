import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="easy_gitlab_pkg_devlifealways",
    version="0.0.3",
    author="ROUINEB Hamza",
    author_email="rouineb.work@gmail.com",
    description="easy gitlab package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/devlifealways/utilities.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
