# MYC store script
This is a simple python script to help new comers/developers configure there environment/tools
with appropriate proxies and so on.
If you wish to understand how it works, just put the following :

```shell
$ ./myc --help 
```

example: 
 if you want to initialize the tool :

```shell
$ ./myc --init 
```

to force recreation : 

```shell
$ ./myc --init -f
```

and that's it, after that, you'll have to reload given tools :
1. Docker:  systemctl daemon-reload && systemctl restart docker
2. If you use ZSH : source ~/.zshrc 
3. If you use BASH: source ~/.bashrc

and that's all, happy coding & remember :

“All our dreams can come true, if we have the courage to pursue them.” Walt Disney

