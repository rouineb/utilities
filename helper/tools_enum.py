from enum import Enum, unique


@unique
class Tools(Enum):
    DOCKER = 'config.json.j2'
    DOCKER_DAEMON = 'http-proxy.conf.j2'
    YARN = '.yarnrc.j2'
    GIT = '.gitconfig.j2'
    APT = 'proxy.j2'


class Interpreter(Enum):
    ZSH = '.zshrc'
    BASH = '.bashrc'
