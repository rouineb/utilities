#!/usr/bin/env bash

ENCODED_ART='ICAgX19fX18gX19fX18gIF8gICAgICAgIF9fX19fX18gX19fX19fICAgICAgICAgIF9fICBfXyAKICAvIF9fX198ICBfXyBcfCB8ICAgICAgfF9fICAgX198ICBfX19ffCAgIC9cICAgfCAgXC8gIHwKIHwgfCAgICB8IHxfXykgfCB8ICAgICAgICAgfCB8ICB8IHxfXyAgICAgLyAgXCAgfCBcICAvIHwKIHwgfCAgICB8ICBfX18vfCB8ICAgICAgICAgfCB8ICB8ICBfX3wgICAvIC9cIFwgfCB8XC98IHwKIHwgfF9fX198IHwgICAgfCB8X19fXyAgICAgfCB8ICB8IHxfX19fIC8gX19fXyBcfCB8ICB8IHwKICBcX19fX198X3wgICAgfF9fX19fX3wgICAgfF98ICB8X19fX19fL18vICAgIFxfXF98ICB8X3wKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAK'

echo $ENCODED_ART | base64 -d

trusted_hosts=(pypi.org pypi.python.org files.pythonhosted.org)
for host in "${trusted_hosts[@]}"; do
    trusted="$trusted --trusted-host='$host'"
done

if [ ! -z "$https_proxy" ]; then
    options="--proxy $https_proxy"
fi

pip3 install $trusted -r requirements.txt $options
