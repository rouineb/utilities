#!/usr/bin/python3

import getopt
import sys
import pyfiglet
from compliance_store import ComplianceConfiguror


def help():
    print("NAME")
    print("\tmyc, cpl, gift, giv")
    print("\nSYNOPSIS")
    print(
        "\t{0} [-hvfi] [-c config_file_path] [--config=config_file_path]".format(sys.argv[0]))
    print("\nDESCRIPTION")
    print("\tThis is a simple script meant for new comers to configure every tool needed for daily work.")
    print("\tThus no longer losing time doing basic configuration. A more in depth look at what it does")
    print("\twill show you that for the time being 5 tools are configured for you :")
    print("\t\t1.yarn.")
    print("\t\t2.git.")
    print("\t\t3.docker.")
    print("\t\t4.apt.")
    print("\t\t5.environment variables through profile interpretor files (bash/zsh).")
    print("\tPlease, note that for this script to work, all the above tools should be installed beforehand.")
    print("\nOptions:")
    print("\t-v, --verbose")
    print("\t\tEnable verbose mode to see more logs.")
    print("\t-h, --help")
    print("\t\tShow this message, which is displayed by default if no option is passed.")
    print("\t-f, --force")
    print("\t\tForce the initialization of the configuration (regenerate everything). If old configuration is found, it will backup everything with .back extension")
    print("\t-i, --init")
    print("\t\tInitialize for the first time the myc configuration, not to forget that it will fail if an already old configuration exists. You can force it with -f/--force")
    print("\t-t, --tool=(YARN|yarn|GIT|git|DOCKER|docker|DOCKER_DAEMON|docker_daemon|SHELL|shell)")
    print("\t\tGenerate proxy configuration for a specific tool")
    print("\t-a, --all")
    print("\t\tChange everything at once, every compatible tool")
    print("\t-e, --environment")
    print("\t\tSet proxy envs into your favorite shell profile file")
    print("\t-c, --config=JSON configuration file path")
    print("\t\tThis is crucial, if you want to specify a custom configuration file, then it should be a valid JSON file with all necessary keys!")
    print("\nAuthor(s):")
    print("\tROUINEB Hamza : rouineb.work@gmail.com")


def usage():
    print(
        "usage:\t./{0} [-hvfi] [-c config_file_path] [--config=config_file_path]".format(sys.argv[0]))


def main(argv):
    try:
        opts, args = getopt.getopt(sys.argv[1:], "haifec:t:", [
                                   "help", "init", "force", "all", "environment" "config=", "tool="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    configuror = ComplianceConfiguror()
    output = None
    verbose = False
    force = False
    tool = ''
    init = False
    all = False
    env = False
    if len(opts) == 0:
        usage()
        sys.exit()
    for o, a in opts:
        if o in ("-h", "--help"):
            help()
            sys.exit()
        elif o in ("-a", "--all"):
            all = True
        elif o in ("-f", "--force"):
            force = True
        elif o in ("-e", "--environment"):
            env = True
        elif o in ("-t", "--tool"):
            tool = a
        elif o in ("-c", "--config"):
            print("Unavailable for the time being !")
            sys.exit()
            # config = a
        elif o in ("-i", "--init"):
            init = True
        else:
            assert False, "unhandled option"

    if init and tool and all and env or init and tool or env and tool or env and init or env and all or all and tool or all and init:
        print("You cannot use these options at the same time !")
        print("Check the help")
        sys.exit()
    elif all:
        configuror.render_tools()
    elif init:
        configuror.default_configuration_generation(force)
    elif tool:
        configuror.render_tool(tool)
    elif env:
        configuror.add_env()


if __name__ == "__main__":
    print(pyfiglet.figlet_format("CPL TEAM"))
    main(sys.argv[1:])
