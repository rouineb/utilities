from os.path import exists, splitext, expanduser, join, dirname
from os import mkdir, makedirs, remove, rename
from json import dump, load, JSONDecodeError
from tools_enum import Tools, Interpreter
from shutil import rmtree
from jinja2 import Template
import datetime


class ComplianceConfiguror:
    CONFIGURATION_FOLDER_NAME = '.compliance'
    CONFIGURATION_FILE_NAME = 'config.json'
    BACKUP_EXT = 'backup'
    DEFAULT_CONFIGURATION = {
        "team": "CPL TEAM",
        "license": "IT",
        "email": "PAR-MYC-SUPPORT@socgen.com",
        "proxy": {
            "HTTP": "proxy-sgt.si.socgen:8080",
            "HTTPS": "proxy-sgt.si.socgen:8080",
            "NO_PROXY": "localhost, 127.0.0.1, .sgithub.com"
        }, "tools": {"GIT": {
            "config": ".gitconfig",
            "name": "",
            "email": ""
        }, "YARN": ".yarnrc", "DOCKER": {
            "config": ".docker/config.json",
            "registry": {
                "url": "registry.bsc.aws.societegenerale.com",
                "password": "ZmFzdGl0cnNjOiB9dDU8WHd+VkEtNzI=",
            }
        }, "DOCKER_DAEMON": "/etc/systemd/system/docker.service.d/http-proxy.conf",
            "APT": "/etc/apt/apt.conf.d/proxy", "SHELL": ".zshrc"}}

    def __init__(self, configuration_folder_name=CONFIGURATION_FOLDER_NAME, configuration_file_name=CONFIGURATION_FILE_NAME):
        self.configuror = None
        self.home = expanduser('~')
        self.configuration_folder_name = configuration_folder_name
        self.configuration_file_name = configuration_file_name

    def generate_config_folder(self, force=False):
        folder_full_name = join(self.home, self.configuration_folder_name)
        if force and exists(folder_full_name):
            # remove existing configuration folder
            rmtree(folder_full_name)
        mkdir(folder_full_name)

    def generate_config_file(self, force=False, config_source=None):
        if config_source is not None:
            conf = self.load(config_source)
        else:
            conf = self.DEFAULT_CONFIGURATION

        file_full_path = join(self.home, self.configuration_folder_name,
                              self.configuration_file_name)
        if force and exists(file_full_path):
            remove(file_full_path)
        # JSON 4627
        with open(file_full_path, 'w') as outfile:
            dump(conf,
                 outfile, indent=2, ensure_ascii=False)

    def default_configuration_generation(self, force=False, config_source=None):
        try:
            self.generate_config_folder(force=force)
            self.generate_config_file(force=force, config_source=config_source)
            print(
                "Don't forget to modify the default configuration file ~/.compliance/config.json with your data !")
        except FileExistsError as ex:
            print('{0} already exists, please either remove it, or use \'force\' option'.format(
                ex.filename))

    @property
    def get_configuror(self):
        """ Lazy loading, not until you request it, it will not be loaded
        """
        if self.configuror is None:
            self.configuror = self.load()
        return self.configuror

    def load(self, source=None):
        if source is not None:
            file_name = source
        else:
            file_name = self.configuration_file_name

        file_full_path = join(self.home, self.configuration_folder_name,
                              self.configuration_file_name)
        try:
            with open(file_full_path, 'r') as input_file:
                return load(input_file)
        except JSONDecodeError as ex:
            print(
                'Error parsing {0}: {1} at {2}, please make sure your JSON file is valid'.format(file_full_path, ex.msg, ex.pos))

    def backup(self, filepath):
        directories = dirname(filepath)
        if exists(filepath):
            rename(filepath, '{0}.{1}'.format(
                filepath, ComplianceConfiguror.BACKUP_EXT))
        elif not exists(directories):
            makedirs(directories)

    def render_tool(self, tool):
        if tool.lower() == 'yarn':
            self.render_file(Tools.YARN)
        elif tool.lower() == 'git':
            self.render_file(Tools.GIT)
        elif tool.lower() == 'docker':
            self.render_file(Tools.DOCKER)
        elif tool.lower() == 'docker_daedmon':
            self.render_file(Tools.DOCKER_DAEMON)
        elif tool.lower() == 'apt':
            self.render_file(Tools.APT)
        elif tools.lower() == 'shell':
            self.add_env()
        else:
            print("Unhandled tool for the time being !")
            exit(-1)

    def render_tools(self):
        self.render_file(Tools.YARN)
        self.render_file(Tools.GIT)
        self.render_file(Tools.APT)
        self.render_file(Tools.DOCKER)
        self.render_file(Tools.DOCKER_DAEMON)
        self.add_env()

    def render_file(self, tool):
        if not isinstance(tool, Tools):
            print('Tool should be an instance of Tools enum')
            SystemExit(-1)
        output_file_path = join(
            self.home, self.get_configuror['tools'][tool.name]['config'] if isinstance(self.get_configuror['tools'][tool.name], dict) else self.get_configuror['tools'][tool.name])
        self.backup(output_file_path)
        content = open(join('templates', tool.value)).read()
        template = Template(content)
        render = template.render(
            generation_date=datetime.datetime.now().strftime("%I:%M%p on %d/%m/%Y"), configuror=self.get_configuror)
        output = open(output_file_path, 'w')
        output.write(render)
        output.close

    def add_env(self):
        interpreters = [Interpreter.BASH, Interpreter.ZSH]
        for interpreter in interpreters:
            file_full_path = join(self.home, interpreter.value)
            self.backup(file_full_path)
            with open(file_full_path, 'a') as input_file:
                for key in self.get_configuror['proxy'].keys():
                    input_file.write("export {0}='{1}'\n".format(key,
                                                                 self.get_configuror['proxy'][key]))


if __name__ == "__main__":
    configuror = ComplianceConfiguror()
    configuror.default_configuration_generation(force=True)
    configuror.render_file(Tools.YARN)
    configuror.render_file(Tools.GIT)
    configuror.render_file(Tools.DOCKER)
    configuror.render_file(Tools.DOCKER_DAEMON)
    configuror.render_file(Tools.APT)
    configuror.add_env()
